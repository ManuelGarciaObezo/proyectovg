﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform pos1, pos2, startPos;
    public float speed;
    Vector3 nextPos;
    bool active = false;
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        nextPos = pos1.position;
        rb = GetComponent<Rigidbody2D>();
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            rb.constraints = RigidbodyConstraints2D.None;
            if (transform.position == pos1.position)
            {
                nextPos = pos2.position;
            }
            if (transform.position == pos2.position)
            {
                nextPos = pos1.position;
            }
            transform.position = Vector3.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);
        } else if (!active)
        {
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

    public void SetActiveTrue()
    {
        active = true;
    }

    public void SetActiveFalse()
    {
        active = false;
    }
}
