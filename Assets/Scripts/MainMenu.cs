﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void playGame()
    {
        SceneManager.LoadScene(1);
    }
    public void goNiveles()
    {
        SceneManager.LoadScene(6);
    }

    public void playNivel2()
    {
        SceneManager.LoadScene(2);
    }
    public void playNivel3()
    {
        SceneManager.LoadScene(3);
    }
    public void playNivel4()
    {
        SceneManager.LoadScene(4);
    }
    public void playNivel5()
    {
        SceneManager.LoadScene(5);
    }

    public void regresar()
    {
        SceneManager.LoadScene(0);
    }
}
