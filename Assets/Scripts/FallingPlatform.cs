﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void OnCollisionEnter2D(Collision2D c) 
    {
        Invoke("DropPlatform", 0.5f);
        Destroy(gameObject, 4f);
    }

    void DropPlatform()
    {
        rb.isKinematic = false;
    }
}
