﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addLife : MonoBehaviour
{
    public LevelManager levelManager;
    AudioSource fuenteAudio;
    public AudioClip sonidoVida;

    // Start is called before the first frame update
    void Start()
    {
        levelManager = FindObjectOfType<LevelManager>();
        fuenteAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D c)
    {
        Debug.Log("Vida extra");
        levelManager.agregarVida();
        sVidaExtra();
        Destroy(gameObject);
        
    }

    void sVidaExtra()
    {
        fuenteAudio.clip = sonidoVida;
        fuenteAudio.Play();
    }
}
