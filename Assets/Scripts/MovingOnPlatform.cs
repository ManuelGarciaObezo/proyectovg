﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingOnPlatform : MonoBehaviour
{
    public GameObject player;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("MovingPlatform")) player.transform.parent = collision.gameObject.transform;
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("MovingPlatform")) player.transform.parent = null;
    }
}
