﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class horizontalMovementR : MonoBehaviour
{
    private Rigidbody2D rb;
    public Vector2 velocity;
    public bool moveRec;
    private float grados;
    private bool lado;

    [Range(1, 10)]
    public float jumpVelocity;
    private bool grounded, golpeo;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        moveRec = false;
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
        lado = true;
        golpeo = false;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKey("e") || Input.GetKey("joystick button 4")) && moveRec)
        {
            Quaternion target = Quaternion.Euler(0, 0, 90);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 5);
        }
        if ((Input.GetKey("r") || Input.GetKey("joystick button 5")) && moveRec)
        {
            Quaternion target = Quaternion.Euler(0, 0, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 5);
        }

        if (moveRec)
        {
            float h;
            if (!golpeo)
            {
                h = Input.GetAxis("Horizontal");

                transform.Translate(
                    h * Time.deltaTime * velocity,
                    Space.World
                    );
            }
            else
            {
                if ((h = Input.GetAxis("Horizontal")) < 0)
                {
                    golpeo = false;
                    transform.Translate(
                    h * Time.deltaTime * velocity,
                    Space.World
                    );
                }
            }
        }

        if (Input.GetKey("m") || Input.GetKey("joystick button 2"))
        {
            moveRec = true;
            rb.constraints = RigidbodyConstraints2D.None;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
        if (Input.GetKey("b") || Input.GetKey("joystick button 0"))
        {
            moveRec = false;
        }
        if (Input.GetKey("n") || Input.GetKey("joystick button 1"))
        {
            moveRec = false;
        }

        if ((Input.GetKeyDown("w") || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKey("joystick button 3")) && grounded == true && moveRec)
        {
            Jump();
        }


    }
    public void freezeRec()
    {
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    private void Jump()
    {
        grounded = false;
        Debug.Log("Salto");
        GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpVelocity;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Puerta")
        {
            golpeo = true;
        }
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Box" || collision.gameObject.tag == "PlayerTria" || collision.gameObject.tag == "PlayerCube" || collision.gameObject.tag == "PlayerRec" || collision.gameObject.tag == "MovingPlatform" || collision.gameObject.tag == "Voladora" || collision.gameObject.tag == "Trancapalanca")
        {
            grounded = true;
            Debug.Log("Suelo");
            if (!moveRec)
            {
                freezeRec();
            }
        }
        if (collision.gameObject.tag == "Trancapalanca")
        {
            rb.constraints = RigidbodyConstraints2D.None;
        }

    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Trancapalanca")
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
    }


}
