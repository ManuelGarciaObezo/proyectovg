﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour
{
    public GameObject explosion, objeto;
    Animator anim, animExplosion;
    public bool oneClick;
    public AudioClip sonidoExplosion;
    AudioSource fuenteAudio;
    bool explotado;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        animExplosion = explosion.GetComponent<Animator>();
        explosion.active = false;
        fuenteAudio = GetComponent<AudioSource>();
        explotado = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "PlayerCube")
        {
            anim.SetBool("goDown", true);
            //Activar explosion
            explosion.active = true;
            animExplosion.Play("Explosion");
            Destroy(objeto);
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "PlayerCube" && !explotado)
        {
            fuenteAudio.clip = sonidoExplosion;
            fuenteAudio.Play();
            explotado = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "PlayerCube")
        {
            if (oneClick)
            {
                return;
            }
            anim.SetBool("goDown", false);
        }

    }
}
