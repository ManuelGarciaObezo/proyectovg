﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambiarDeNivel : MonoBehaviour
{
    // Start is called before the first frame update

    public AudioClip sonidoPortal;
    public GameObject musicaFondo;

    AudioSource fuenteAudio, musicFondo;
    void Start()
    {
        fuenteAudio = GetComponent<AudioSource>();
        musicFondo = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        cambiarNivel();
    }
    public void cambiarNivel()
    {
        fuenteAudio.clip = sonidoPortal;
        fuenteAudio.Play();
        StartCoroutine("Esperar");
        
    }
    IEnumerator Esperar()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(2);

    }
}
