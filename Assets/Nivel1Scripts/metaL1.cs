﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class metaL1 : MonoBehaviour
{
    public GameObject GanasteUI;
    void OnTriggerEnter2D(Collider2D other)
    {
        mostrarGanaste();
    }

    void mostrarGanaste()
    {
        GanasteUI.SetActive(true);
        Time.timeScale = 0f;
    }
}
