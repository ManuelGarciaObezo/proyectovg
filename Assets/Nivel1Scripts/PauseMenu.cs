﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPause = false;

    public GameObject pauseMenuUI;

   void Update()
    {
        if (Input.GetKeyDown("p"))//pause
        {
            if (gameIsPause)
            {
                Resume();

            }
            else
            {
                Pause();
            }
        }
    }
    public void Resume ()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPause = false;
    }
    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPause = true;
    }
    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }
    public void QuitGame()
    {
        Debug.Log("I want to quit");
        Application.Quit();
    }
    public void Reiniciar()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
    }

    public void ReiniciarNivel2()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(2);
    }
    public void ReiniciarNivel3()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(3);
    }
    public void ReiniciarNivel4()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(4);
    }
    public void ReiniciarNivel5()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(5);
    }
    public void SiguienteNivel()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(2);
    }
    public void SiguienteNivel2()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(3);
    }
    public void SiguienteNivel3()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(4);
    }
    public void SiguienteNivel4()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(5);
    }
    public void JugarDeNuevo()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
    }

}
