﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class botonVerdeScript : MonoBehaviour
{
    public GameObject mecanismo;
    Animator anim;
    public bool oneClick;
    Rigidbody2D rb;
    bool active = false;
    public MovingPlatform mp;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        if (mecanismo.CompareTag("MovingPlatform"))
        {
            mp = mecanismo.GetComponent<MovingPlatform>();
        }
    }

    // Update is called once per frame
    void Update()
    {
    
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "PlayerTria")
        {
            if (mecanismo.CompareTag("MovingPlatform")) mp.SetActiveTrue();
                anim.SetBool("goDown", true);
            //Activar mecanica
            if (mecanismo.CompareTag("Puerta")) Destroy(mecanismo.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "PlayerTria")
        {
            if (mecanismo.CompareTag("MovingPlatform")) mp.SetActiveFalse();
            if (oneClick)
            {
                return;
            }
            anim.SetBool("goDown", false);
        }

    }
}
